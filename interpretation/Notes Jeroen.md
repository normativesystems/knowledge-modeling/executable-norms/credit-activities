## National Consumer Credit Protection Act 2009

### Provenance

The latest version of the act can be viewed at https://www.legislation.gov.au/Details/C2023C00020 .

### Analysis

- Act: Person engages in credit activity
    - Actor: Person
    - Recipient: 
        - "consumer": EITHER
            - Natural person
            - Strata organisation
            <!-- NCCA Vol 1 Part 1-2 Division 2 Section 5 definition of consumer; Also Divison 3 Section 6-7 definition of credit activity / credit service as targeting consumers ; Also NCCA Vol 2 Schedule 1 Part 1 5(1)(a) -->
    - Object: -
    - Action: engage in credit activity
    - General preconditions:
        - EITHER
            - Actor has a license authorising them to engage in the credit activity
            - ALL OF
                - Actor acts on behalf of a principal
                - Actor's conduct in engaging in the credit activity is within the authority of the principal
                - The principal holds a licence authorising the principal to engage in the credit activity
                - EITHER
                    - Actor is an employee or director of the principal
                    - Actor is an employee or director of a body corporate relative to the principal
                    - Actor is a credit representative of the principal
    - Postconditions: -
- Act: Apply civil penalty
    - Preconditions:
        - (TBD?)


### Text

29 Prohibition on engaging in credit activities without a licence

Prohibition on engaging in credit activities without a licence

(1) A person must not engage in a credit activity if the person does not hold a licence authorising the person to engage in the credit activity.
Civil penalty:
5,000 penalty units.

Offence
(2) A person commits an offence if:
(a) the person is subject to a requirement under subsection (1); and
(b) the person engages in conduct; and
(c) the conduct contravenes the requirement.
Criminal penalty:
2 years imprisonment.

Defences
(3) For the purposes of subsections (1) and (2), it is a defence if:
(a) the person engages in the credit activity on behalf of another
person (the principal); and
(b) the person is:
(i) an employee or director of the principal or of a related body corporate of the principal; or
(ii) a credit representative of the principal; and
(c) the person’s conduct in engaging in the credit activity is within the authority of the principal; and
(d) the principal holds a licence authorising the principal to engage in the credit activity.

