# CONTEXT
- The source: Article 29 of the [National Consumer Credit Protection Act 2009](http://classic.austlii.edu.au/au/legis/cth/consol_act/nccpa2009377/) (National Credit Act).
- This act is part of the "Commonwealth of Australia Consolidated Acts"
- **Australia**, officially the **Commonwealth of Australia**, is a [sovereign](https://en.wikipedia.org/wiki/Sovereign_state "Sovereign state") country comprising the mainland of the [Australian continent](https://en.wikipedia.org/wiki/Australia_(continent) "Australia (continent)"), the island of [Tasmania](https://en.wikipedia.org/wiki/Tasmania "Tasmania"), and numerous [smaller islands](https://en.wikipedia.org/wiki/List_of_islands_of_Australia "List of islands of Australia").

# Interpretation

-   Act: person engages in credit activity
	- agents
		- actor: person
		- recipient: {?}
	- action: engage
	- object: -
    -   Precondition (or conditions):
          (
          Actor has a license
          OR
             (
             Actor acts on behalf of a principal
             AND
             (
                 Actor is an employee or director of the principal
                 OR
                 Actor is an employee or director of a body corporate relative to the principal
                 OR
                 Actor is a credit representative of the principal
              )
              AND
              principal holds a licence authorising the principal to engage in the credit activity

-   Act: filing a Statement of Claim against a person engaging in credit activity without a licence 
	- agents
		- actor: {?}
		- recipient: a civil court 
	- action: filing
	- object: credit activity without a licence
    -   Precondition (or conditions):
          (
          NOT person engaged in credit activity has a license
          OR
             (
             person engaged in credit activity acts on behalf of a principal
             AND
             (
                 NOT person engaged in credit activity is an employee or director of the principal
                 OR
                 NOT person engaged in credit activity is an employee or director of a body corporate relative to the principal
                 OR
                 NOT person engaged in credit activity is a credit representative of the principal
                 OR
                 NOT principal holds a licence authorising the principal to engage in the credit activity
    -   Postcondition:
        - person engaged in credit activity without a licence gets 5,000 penalty units

-   Act: prosecution person engaging in credit activity without a licence 
	- agents
		- actor: State or the Crown or prosecution or or prosecutor or Commonwealth of Australia
		- recipient: defendant
	- action: prosecute
	- object: credit activity without a licence
    -   Precondition (or conditions):
          (
          NOT person engaged in credit activity has a license
          OR
             (
             person engaged in credit activity acts on behalf of a principal
             AND
             (
                 NOT person engaged in credit activity is an employee or director of the principal
                 OR
                 NOT person engaged in credit activity is an employee or director of a body corporate relative to the principal
                 OR
                 NOT person engaged in credit activity is a credit representative of the principal
                 OR
                 NOT principal holds a licence authorising the principal to engage in the credit activity
    -   Postcondition:
        - person engaged in credit activity without a licence gets penalty of 2 years imprisonment

# COMMENT
1. It is not clear what the client should be named.
2. The procedures for civil and criminal lawsuits in Australia is based on a quick internet search, for proper analyses this needs to be substantiated by sources.
3. Probably there are separate decision on:
	1. the claim in the civil case and the penalty (it might not always be 5,000 penalty units or maybe it is, but we don't know)
	2. the verdict (is the defendant guilty) and the sentencing (what is the penalty, probably it is not always 2 years imprisonment, that probably is the maximum sentence)
	3. We used the [Civil procedure code](https://legislation.nsw.gov.au/view/html/inforce/current/act-2005-028#statusinformation) of New South Wales (because this was the one on top in a Google search)
	4. There are multiple [civil courts](https://legislation.nsw.gov.au/view/html/inforce/current/act-2005-028#sch.1), I don't know what the correct one is in these type of cases.

### [](#text)Text

29 Prohibition on engaging in credit activities without a licence

Prohibition on engaging in credit activities without a licence

(1) A person must not engage in a credit activity if the person does not hold a licence authorising the person to engage in the credit activity. Civil penalty: 5,000 penalty units.

Offence (2) A person commits an offence if: (a) the person is subject to a requirement under subsection (1); and (b) the person engages in conduct; and (c) the conduct contravenes the requirement. Criminal penalty: 2 years imprisonment.

Defences (3) For the purposes of subsections (1) and (2), it is a defence if: (a) the person engages in the credit activity on behalf of another person (the principal); and (b) the person is: (i) an employee or director of the principal or of a related body corporate of the principal; or (ii) a credit representative of the principal; and (c) the person’s conduct in engaging in the credit activity is within the authority of the principal; and (d) the principal holds a licence authorising the principal to engage in the credit activity.