A study of a snippet of the Australian National Consumer Credit Protection Act 2009, as discussed in Francesconi & Governatori (2022).

The latest version of the legislation can be viewed at https://www.legislation.gov.au/Details/C2023C00020 .

This repository is structured to be used as a project by the [Calculemus Calculator](https://gitlab.com/normativesystems/knowledge-modeling/executable-norms/calculemus-calculator). See `/interpretation` for notes on the interpretation of the relevant legislation.
